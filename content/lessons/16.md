---
title: "Shape"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "16"
puzzles:
- id: pzl1
  target: shapes_of_2
  text: Shapes of 2
- id: pzl2
  target: shapes_of_3
  text: Shapes of 3
- id: pzl3
  target: shapes_of_4
  text: Shapes of 4
returnTo: "lessons/17"
section: "index.html#strategies"
---

# | Make Some Shape
## Even your stones need to be in shape

> Being human, we are unable (and too lazy) to read out every possible variation for our stones. Thus playing in such a way that we can be reasonably sure is safe and efficient might be a good idea.  

**I can't talk about every shape in depth here, so what we will do is just try to introduce the concept (so you know what we are talking about when we talk about shape) and offer you some further reading should you be interested.**

Any stronger player minds the shape of his groups. By shape we mean the formations a group of your stones have and the relation between them.

Now you can be pretty sure that two solidly connected stones will never be separated. However, while it is a good shape, being so close to one another they may not be efficient enough. Thus, we keep an eye for many shapes and try to choose the best one to fit the situation, not the other way around.

**A good shape should do as much of the following as possible:**

{{< rule >}}
	Maintain a {{< black "connection" >}} (because it is usually easier to  take care of one group rather than two)
{{< /rule >}}

{{< rule >}}
	Be {{< black "efficient" >}} (you want the best possible effect with the least possible moves)
{{< /rule >}}

{{< rule >}}
	Create {{< black "eye potential" >}} (so you can live easily)
{{< /rule >}}
 
{{< rule >}}
	Maximize {{< black "liberties" >}} (so you can win capturing races)
{{< /rule >}}

{{< sgf >}}

There are good shapes, there are bad shapes and there are shapes that are sometimes good and sometimes not good enough. There are also bad shapes that can sometimes be a good move anyway. You get what I am saying? Shapes help us narrow down the variations we need to read. They do not solve problems by themselves. Do not aim to create shapes out of nothing though. Figure out what your aim is and then chose a shape to fit the situation. I did already say that but it is REALLY important. 

**Further reading:**
<a href="https://cdn.online-go.com/shape_up.pdf" target="_blank" noreferrer noopener><u>Shape Up - The book of shape</u></a>
