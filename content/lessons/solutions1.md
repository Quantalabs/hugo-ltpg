---
title: "Placing the Stones"
date: 2019-03-31T10:07:35+02:00
draft: false
cookieSetting: "s1"
returnTo: "index.html#rules"
batch: "1"
puzzles:
- id: pzl1
  target: 1/solutions/0
  text: Atari 1
- id: pzl2
  target: 1/solutions/1
  text: Atari 2
- id: pzl3
  target: 1/solutions/2
  text: Illegal moves
- id: pzl4
  target: 1/solutions/3
  text: Edge of board

---

# | Solutions to quiz #1
## It’s fine to celebrate success but it is more important to heed the lessons of failure.<br><br> 

Here, the puzzles you have just tried are explained. The ones you have failed are highlighted red.

{{< tsumego show="true" >}}

{{< quizDisplay >}}