---
title: "Placing the Stones"
date: 2019-03-31T10:07:35+02:00
draft: false
cookieSetting: "1"
returnTo: "lessons/02"
section: "index.html#rules"
---

# | Placing the Stones
## Hard to master, but easy to learn

> Go is one of the world's great strategy board games played for thousands of years. While it is infamously hard to master all the intricate strategies, learning the rules is easy and should not take more than a few minutes. It can be enjoyed at any level of ability so these rules are really all you need to fully enjoy a match!

Go is usually played on a square grid of 19x19 or 9x9 lines with a set of black stones for one player and white stones for the other. The aim of the game is to get more points than your opponent. **Players get points for surrounding territory and by capturing the opponent's stones** (which will be covered soon in following lessons).

Feel free to test out a few moves for both Black and White, just to get the basic feel for it. 

{{< jgoboard >}}

{{< rule >}}
The stones are played on the {{< black "intersections." >}} Not inside the squares.
{{< /rule >}}

{{< rule >}}
{{< black "Black" >}} plays first and then White and Black take turns.
{{< /rule >}}

{{< rule >}}
Once a stone is played, it doesn’t move, but can be removed when {{< black "captured">}}.
{{< /rule >}}

The first step is behind you, and we will talk about capturing in the next chapter.
More extra background information is below if you are interested.


{{< extra "1" "First move advantage and komi" >}}
	Black has an advantage by being able to make the first move. To balance this out, White gets some extra points (called komi). 
    It is usually 6.5 points on the 19x19 board and can vary on 9x9 depending on where you play (7.5 being currently one of the best estimates). The half point prevents ties.
{{< /extra >}}

{{< extra "3" "A brief history of Go" >}}
	Go is believed to be the oldest board game in the world still being played in its original form.<br><br>
	
	It most probably originated in China some 3,000 or 4,000 years ago. The game then spread to Japan, Korea, and much, much later to the rest of the world.<br><br>
	
	For the longest time, Go was the last game in which human masters played better than even the most sophisticated programs. It wasn't until 2016 and the invention of neural networks like AlphaGo that computers finally overtook humans.<br><br>
	
	Go is gaining popularity in western countries and many professional organizations are being established across Europe and the rest of the world.
{{< /extra >}}
